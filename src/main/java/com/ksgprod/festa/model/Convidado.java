package com.ksgprod.festa.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Convidado implements Serializable {

	private static final long serialVersionUID = 5529797067576612531L;

	private String id;

	private String nome;

	private Integer quantidadeAcompanhantes;

	public Convidado(String id, String nome, Integer quantidadeAcompanhantes) {
		super();
		this.id = id;
		this.nome = nome;
		this.quantidadeAcompanhantes = quantidadeAcompanhantes;
	}

	public Convidado() {
		super();
	}

}
