package com.ksgprod.festa.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.ksgprod.festa.model.Convidado;

@Controller
@RequestMapping("/convidados")
public class ConvidadosController {
	
	private static List<Convidado> convidados = new ArrayList<>();

	@GetMapping
	public ModelAndView listar() {
		ModelAndView modelAndView = new ModelAndView("ListaConvidados");
		
		modelAndView.addObject("convidados", convidados);
		modelAndView.addObject(new Convidado());
		return modelAndView;
	}
	
	@PostMapping
	public String salvar(Convidado convidado) {
		convidados.add(convidado);
		return "redirect:/convidados";
	}
	
	static {
		convidados.add(new Convidado("1", "João", 2));
		convidados.add(new Convidado("2", "Maria", 3));
		convidados.add(new Convidado("3", "José", 1));
	}
}
